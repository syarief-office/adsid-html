var gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefix = require('gulp-autoprefixer'),
  jsHint = require('gulp-jshint'),
  uglify = require('gulp-uglify'),
  imageMin = require('gulp-imagemin'),
  cache = require('gulp-cache'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename'),
  del = require('del'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  prettify = require('gulp-jsbeautifier'),
  fileinclude = require('gulp-file-include'),
  browserSync = require('browser-sync').create();


gulp.task('html', function() {
  return gulp
    .src('*.html')
    .pipe(prettify())
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('dist'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('styles', function() {
  return gulp
    .src('assets/scss/**/*.scss')
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(sass({outputStyle: 'expanded'}))
    // .pipe(sourcemaps.init())
    .pipe(autoprefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    // .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('scripts', function() {
  return gulp
    .src(['assets/js/*.js', 
          'bower_components/vue/dist/vue.js',
          'assets/js/**/*.js'
        ])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(jsHint())
    .pipe(jsHint.reporter('default'))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('concatapp', function() {
  return gulp
    .src(['assets/app/**/*.js',])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(concat('app.js'))
    .pipe(gulp.dest('dist/assets/app'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/app'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('concatlibs', function() {
  return gulp
    .src([
      'bower_components/jquery/dist/jquery.js',
      'bower_components/jquery-ui/jquery-ui.js',
      'bower_components/sweetalert/dist/sweetalert-dev.js'
    ])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        this.emit('end');
      }
    }))
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('dist/assets/js'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('images', function() {
  return gulp
    .src('assets/img/*')
    .pipe(cache(imageMin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest('dist/assets/img'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('icons', function() {
  return gulp
    .src('assets/fonts/*')
    .pipe(gulp.dest('dist/assets/fonts'))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('serve', function() {
  browserSync.init({
    notify: false,
    server: {
      baseDir: 'dist/'
    }
  });
});

gulp.task('clean', function() {
  return del(['dist/assets/**']);
});

gulp.task('default', ['html', 'styles', 'scripts', 'images', 'icons', 'concatapp', 'concatlibs'], function() {
  gulp.watch('*.html', ['html']).on('change', browserSync.reload);
  gulp.watch(['assets/scss/**/*.sass', 'assets/scss/**/*.scss'],['styles']);
  gulp.watch('assets/js/**/*.js', ['scripts', 'concatlibs']);
  gulp.watch('assets/app/**/*.js', ['concatapp']);
  gulp.watch('assets/img/*', ['images']);
  gulp.watch('assets/img/*', ['images']);
  gulp.start('serve');
});

gulp.task('watch', function () {
  browserSync.init({
    server: "./dist",
    logFileChanges : false
  })
});
