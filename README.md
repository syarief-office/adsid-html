http://www.rainatspace.com

My-Gulp-Boilerplate
====================

**File Structure**


#### **Installations**
Require NodeJS installed on your machine.

#### **Install Gulp **
> **`npm install -g gulp-cli`**
> or you can look at this http://gruntjs.com/getting-started for detail

#### **Install NodeJS Modules**
> **`npm install`**

#### **Install Bower**
> **`npm install -g bower`**
> **`npm install`**
> Tutorial : http://blog.teamtreehouse.com/getting-started-bower

#### **HTML INCLUDE**
> **`https://www.npmjs.com/package/gulp-file-include`**

#### **Compile**
> **`gulp`**

***
