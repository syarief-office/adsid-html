//Animate Method
jQuery.fn.extend({
	animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName + ' show').one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },

    activeState : function() {
    	$(this).on('click', function (e) {
    		e.preventDefault();
    		$(this).toggleClass('active');
    	});
    }
});

//Method DOM
var base = {
	initialize : function() {
		this.events();
		this.navigate();
		this.popup();
		this.initPlugin();
	},
	
	events : function(){
	  	jQuery('.btn-src').on('click', function () {
	  		jQuery(this).toggleClass('active');
	    	jQuery('.search-box').toggleClass('show fadeInUp');
	    });

	    jQuery('.user-rate span').on('click', function () {
	    	jQuery(this).toggleClass('choose');
	    });

	    jQuery('.js-cxToggle').on('click', function () {
	    	$target = jQuery(this).closest('.box-payment').find('.desc-content');
	    	jQuery('.desc-content').stop().slideUp();
	    	$target.stop().slideDown();
	    });

	    $('.js-toggle').activeState();

		   //customized
		$('.tabs_wrapper .tab_content:first').show();
		$('.tabs_wrapper .tabs-nav li').click(function() {
			var $panel = $(this).closest('.tabs_wrapper'),
				selected_tab = $(this).attr("rel");
			$panel.find(".tabs-nav li").removeClass('active');
			$(this).addClass("active");
			$(".tab_content").hide();
			$panel.find('.' + selected_tab).slideDown('fast');
			return false;
		});

		$('.js-vote').activeState();

		$(window).on('resize', function () {
			if($('.sidebar-right').length) {
				var winWidth = $(window).width();
				if (winWidth < 767) {
					$('.comments').insertAfter($('.sidebar-right'));
				};
			}
		});
		$(window).resize();
	},

	navigate : function() {
		 
		 // NAVIGATION RESPOSNIVE HANDLER
	    jQuery(".mainMenu").clone(false).appendTo("#mobileNav").removeClass();
	    var mobileNav = jQuery('#mobileNav'),
	        shadow = jQuery('.js-nav-shadow');
	    jQuery("#mobileNav li").each(function () {
	      if (jQuery('ul', this).length == 1) {
	        jQuery(this).append('<i class="fa fa-angle-down" aria-hidden="true"></i>');
	        jQuery('ul', this).addClass('sub-menuNav');
	        jQuery(this).click(function () {
	          jQuery('ul', this).slideToggle();
	        });
	      }
	    });

	    jQuery(".js-btn-mobileNav").click(function () {
	      mobileNav.animateCss('slideInLeft');
	      jQuery('body').addClass('nav-opened');
	      shadow.fadeIn('fast');
	      return false;
	    });

	    jQuery(document).click(function () {
	      var target = jQuery(event.target);
	      if (target.is('.js-nav-shadow')) {
	        mobileNav.removeClass('show');
	        $('body').removeClass('nav-opened');
	        shadow.fadeOut('fast');
	      }
	    });

	    jQuery('.close-sidebar').on('click', function () {
	      mobileNav.removeClass('show');
	      jQuery('body').removeClass('nav-opened');
	      shadow.fadeOut('fast');
	    });

	    //Zebra Table 
	    if(jQuery('.rTable--stripe').length){
		    jQuery(".rTable--stripe .rTable-cell").each( function() {
			  var order = jQuery(this).attr("style").match(/order\s*:\s*\d/g)[0].replace(/order(\s*):(\s*)/,"");
			  if(order % 2 === 0) $(this).addClass("is-striped");
			});
		};
	},

	initPlugin : function () {
		//Custom Select
		jQuery('.selectStyle').customSelect();
		jQuery('.tip').tipr();
	},

	popup : function () {

		if ($('.modal-box').length) {
			 var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
			 $('a[data-modal-id]').click(function(e) {
			    e.preventDefault();
			    $("body").append(appendthis);
			    $(".modal-overlay").fadeTo(500, 0.7);
			    //$(".js-modalbox").fadeIn(500);
			    var modalBox = $(this).attr('data-modal-id');
			    $('#'+modalBox).fadeIn($(this).data());
			  });  
			  
			$(".js-modal-close, .modal-overlay").click(function() {
				$(".modal-box, .modal-overlay").fadeOut(500, function() {
				$(".modal-overlay").remove();
				});
			});

			//MAKE THE MODAL CENTERED
			$(window).resize(function() {
			    $(".modal-box").css({
			        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
			        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
			    });
			});
			$(window).resize();
		}
	} 
};

jQuery(window).on('load', function () {
	base.initialize();
});

